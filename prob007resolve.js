 
console.log("FUNCTIONS");
function hi()                               //FUNCTION DECLERATION....
	{console.log("solving problems");};
console.log(hi.toString());
hi();                                        //FUNCTION CALL....
//.....................................................
	console.log("CREATING ANOTHER FUNCTION NAMED Add")
	function add(a,b){
		var add = a+b; console.log(add);
	};	
console.log(add.toString());
add(3,5);
//.............................................................
function sub(a,b){
	var c = a-b;console.log(c);
};
sub(60,10);
console.log(sub.toString());
console.log("............{add(10,20);sub(50,40);};.......................................")
{add(10,20);sub(50,40);};

//............now we are trying for the nested function........
 function nested (){add("hello","jay");
};
nested();
//.....................................................
console.log("*---------------------------------------------*");
//.....the function within the another function automatically has accses to the variable declared within its parent function..
function fullName() {
var firstName = "Aditya";
function alertFullName() {
var lastName = "Jadhav";
console.log("Full name: " + firstName + " " + lastName);
}
alertFullName();// it is called first ,in this function, he has only last name.(local scope so hearch the variable in its parent function .
}
console.log(fullName.toString());
fullName();
//.....now we will be studying ,in function, how the value will be passes..
function perso(firstName, lastName, age) {
	console.log(firstName),console.log(lastName),console.log(age);
};

console.log(perso.toString());
perso("John", "Doe", 44);
//....................................................................




